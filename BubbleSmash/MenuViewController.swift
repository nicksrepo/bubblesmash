//
//  MenuViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 30/4/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit
import CoreData

class MenuViewController: UIViewController {
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Data", withExtension: "momd")
        return NSManagedObjectModel(contentsOf: modelURL!)!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
