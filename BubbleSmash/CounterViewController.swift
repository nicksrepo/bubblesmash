//
//  CounterViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 17/5/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit

class CounterViewController: UIViewController {
    
    @IBOutlet weak var counterLabel: UILabel!
    
    var activeTimer = Timer()
    
    var timer = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activeTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    func timerAction() {
        timer -= 1
        
        if(timer == 0){
            counterLabel.text = "Start!"
        }
        if(timer == -1){
            activeTimer.invalidate()
            self.performSegue(withIdentifier: "afterCounterSegue", sender:  self)
        }
        if(timer > 0){
            counterLabel.text = "\(timer)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
