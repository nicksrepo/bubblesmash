//
//  BubbleGameScene.swift
//  BubbleSmash
//
//  Created by Nickita on 29/4/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import SpriteKit
import UIKit

struct GameValues {
    static var timer = 0
    static var bubbles = 0
    static var viewController: UIViewController!
    static var playerName = ""
}

class BubbleGameScene: SKScene {
    
    var touchLocation:CGPoint = CGPoint.zero
    var bubbleColors = ["bubble-red", "bubble-blue", "bubble-pink", "bubble-green", "bubble-black"]
    let screenSize = UIScreen.main.bounds
    var bubbles = [SKSpriteNode]()
    var score = 0
    var scoreLabel: SKLabelNode!
    var timerLabel: SKLabelNode!
    var highLabel: SKLabelNode!
    var playerName:String!
    var counter = 0
    var activeTimer = Timer()
    var highScore = 0
    //Combo features
    var lastBubbleColor = ""
    var multiplyCombo = 1
    var comboFireParticle: SKEmitterNode!
    
    override func didMove(to view: SKView) {
        let userDefaults = UserDefaults.standard
        highScore = userDefaults.integer(forKey: "highScore")
        scoreLabel = self.childNode(withName: "scoreLabel") as! SKLabelNode!
        timerLabel = self.childNode(withName: "timerLabel") as! SKLabelNode!
        highLabel = self.childNode(withName: "highLabel") as! SKLabelNode!
        comboFireParticle = self.childNode(withName: "comboFireParticle") as! SKEmitterNode!
        comboFireParticle.isHidden = true
        
        highLabel.text = "\(highScore)"
        
        let randomNumberAdd = Int(arc4random_uniform(UInt32(GameValues.bubbles)))
        
        for i in (0..<randomNumberAdd) {
            spawnBubble(i: i)
        }
        
        counter = GameValues.timer;
        activeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let location = (touch as UITouch).location(in: self)
            let node = self.atPoint(location)
            let name = node.name!.components(separatedBy: "-")
            let namet = node.name!
            print(namet)
            if name[0] == "bubble"
            {
                print(score)
                switch name[1] {
                case "green":
                    if(lastBubbleColor == name[1]){
                        multiplyCombo = 2
                    }else{
                        multiplyCombo = 1
                    }
                    score += 5 * multiplyCombo
                    scoreLabel.text = "\(score)"
                    lastBubbleColor = name[1]
                    break
                case "red":
                    if(lastBubbleColor == name[1]){
                        multiplyCombo = 2
                    }else{
                        multiplyCombo = 1
                    }
                    score += 1 * multiplyCombo
                    scoreLabel.text = "\(score)"
                    lastBubbleColor = name[1]
                    break
                case "blue":
                    if(lastBubbleColor == name[1]){
                        multiplyCombo = 2
                    }else{
                        multiplyCombo = 1
                    }
                    score += 8 * multiplyCombo
                    scoreLabel.text = "\(score)"
                    lastBubbleColor = name[1]
                    break
                case "pink":
                    if(lastBubbleColor == name[1]){
                        multiplyCombo = 2
                    }else{
                        multiplyCombo = 1
                    }
                    score += 2 * multiplyCombo
                    scoreLabel.text = "\(score)"
                    lastBubbleColor = name[1]
                    break
                case "black":
                    if(lastBubbleColor == name[1]){
                        multiplyCombo = 2
                    }else{
                        multiplyCombo = 1
                    }
                    score += 10 * multiplyCombo
                    scoreLabel.text = "\(score)"
                    lastBubbleColor = name[1]
                    break
                default:
                    break
                }
                
                deleteBubble(b: node as! SKSpriteNode)
                
                if multiplyCombo == 2 {
                    comboFireParticle.isHidden = false
                }else{
                    comboFireParticle.isHidden = true
                }
                
                if score > highScore {
                    highLabel.text = "\(score)"
                }
            }
            
            
        }
        
    }
    
    func deleteBubble(b:SKSpriteNode){
        for i in 0...bubbles.count - 1{
            if
                bubbles[i] == b{
                b.removeFromParent()
                bubbles.remove(at: i)
                return
            }
        }
    }
    
    func spawnBubble(i:Int){
        let bubble: SKSpriteNode = SKScene(fileNamed:"Bubble")?.childNode(withName: "bubble") as! SKSpriteNode
        bubble.removeFromParent()
        let randomX = Int(arc4random_uniform(UInt32(screenSize.width)))
        let randomY = Int(arc4random_uniform(UInt32(screenSize.height)))
        
        var ptexture = 0
        
        let randomNumber = Double(arc4random() % 1000) / 10.0
        switch(randomNumber) {
        case 0..<40:
            ptexture = 0
        case 40..<70:
            ptexture = 2
        case 70..<85:
            ptexture = 3
        case 85..<95:
            ptexture = 1
        default:
            ptexture = 4
        }
        
        let texture = bubbleColors[ptexture]
        
        bubble.texture = SKTexture(imageNamed: texture)
        
        bubble.name = "\(bubbleColors[ptexture])"
        bubble.position = CGPoint(x:randomX, y:randomY)
        
        addChild(bubble)
        
        bubbles.append(bubble)
        
        for i in 0..<bubbles.count{
            moveBubble(bubble: bubbles[i])
        }
        
    }
    
    func moveBubble(bubble:SKSpriteNode){
        let randomForce = (CGFloat(arc4random()) / CGFloat(UInt32.max) - 0.5) * 2.0
        let vector:CGVector = CGVector(dx:randomForce*1000, dy:randomForce*1000);
        bubble.physicsBody?.applyForce(vector)
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        for i in 0..<bubbles.count{
            moveBubble(bubble: bubbles[i])
        }
    }
    
    func timerAction() {
        
        timerLabel.text = "\(counter)"
        
        counter -= 1
        
        let randomNumberAdd = Int(arc4random_uniform(UInt32(GameValues.bubbles - bubbles.count + 1)))
        
        for i in (0..<randomNumberAdd) {
            spawnBubble(i: i)
        }
        
        let randomNumberDelete = Int(arc4random_uniform(UInt32(bubbles.count)))
        
        
        for _ in 0..<randomNumberDelete{
            var randomBubbleId = Int(arc4random_uniform(UInt32(bubbles.count - 1)))
            
            if(randomBubbleId<0){
                randomBubbleId += 1
            }
            
            deleteBubble(b: bubbles[randomBubbleId])
        }
        
        if counter<0{
            endGame()
        }
    }
    
    func endGame(){
        GameOverValues.passedScore = score
        print("Passed score: \(GameOverValues.passedScore)")
        activeTimer.invalidate()
        GameValues.viewController!.performSegue(withIdentifier: "toScoreSegue2", sender:  self)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchLocation = touches.first!.location(in: self)
    }
    
}
