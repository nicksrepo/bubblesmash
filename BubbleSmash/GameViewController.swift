//
//  GameViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 29/4/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        GameValues.viewController = self

        if let scene = BubbleGameScene(fileNamed:"BubbleGameScene") {
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            

            skView.ignoresSiblingOrder = true
            
            scene.scaleMode = .aspectFill
            
            skView.presentScene(scene)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
