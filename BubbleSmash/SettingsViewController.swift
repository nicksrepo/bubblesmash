//
//  SettingsViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 30/4/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var playerEditText: UITextField!
    
    @IBOutlet weak var gameLengthLabel: UILabel!
    
    @IBOutlet weak var numberOfBubblesLabel: UILabel!
    
    @IBOutlet weak var bubblesNumberSlider: UISlider!
    
    @IBOutlet weak var lengthSlider: UISlider!
    
    
    var gameLength = 60
    var bubblesNumber = 1
    
    var playerName:String? = "Player"
    
    @IBAction func onEditTextPressed(_ sender: Any) {
        playerEditText.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameLengthLabel.text = "\(Int(lengthSlider.value))"
        gameLength = Int(lengthSlider.value)
        self.playerEditText.delegate = self;
        numberOfBubblesLabel.text =
        "\(Int(bubblesNumberSlider.value))"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onStartButtonClicked(_ sender: Any) {
        
        if let text = playerEditText.text, !text.isEmpty
        {
            GameValues.timer = gameLength
            GameValues.bubbles = bubblesNumber
            GameValues.playerName = playerEditText.text!
        }else{
            let alert = UIAlertController(title: "Ooops!", message: "Please, enter your player name. Don't be shy... Being modest will not help you to win this game.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Will do it now", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func lengthSliderClicked(_ sender: UISlider) {
        gameLengthLabel.text = "\(Int(lengthSlider.value))"
        gameLength = Int(lengthSlider.value)
    }
    
    @IBAction func onBubbleNumberChanged(_ sender: Any) {
        bubblesNumber = Int(bubblesNumberSlider.value)
        numberOfBubblesLabel.text = "\(bubblesNumber)"
    }
    
    
}
