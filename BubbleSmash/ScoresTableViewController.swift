//
//  ScoresTableViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 5/5/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit
import RealmSwift

class ScoresTableViewController: UITableViewController {
    
    @IBOutlet var tableViewMain: UITableView!
    
    var scores: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadScoresFromRealm()
    }

    func loadScoresFromRealm(){
        scores.removeAll()
        let realm = try! Realm()
        let scoresRL = realm.objects(Score.self).sorted(byKeyPath: "score", ascending: false)
        for score in scoresRL {
            scores.append("\(score.name) : \(score.score)")
        }
    }
    
    func resetScoresRealm(){
        let realm = try! Realm()
        let scoresRL = realm.objects(Score.self)
        for score in scoresRL {
            try! realm.write {
                realm.delete(score)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onResetButtonClicked(_ sender: Any) {
        resetScoresRealm()
        loadScoresFromRealm()
        tableViewMain.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scores.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        cell.textLabel?.text = scores[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Scores:"
    }

}
