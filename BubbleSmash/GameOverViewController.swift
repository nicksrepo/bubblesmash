//
//  GameOverViewController.swift
//  BubbleSmash
//
//  Created by Nickita on 2/5/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

struct GameOverValues{
    static var passedScore = 0
}

class Score:Object{
    dynamic var name = ""
    dynamic var score = 0
}

class GameOverViewController: UIViewController {
    
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var yourScoreLabel: UILabel!
    
    var intHighScore = 0
    var userDefaults: UserDefaults!
    
    @IBAction func onResetScoreButtonClicked(_ sender: Any) {
        userDefaults.set(0, forKey: "highScore")
        highScoreLabel.text = "\(0)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDefaults = UserDefaults.standard
        
        intHighScore = userDefaults.integer(forKey: "highScore")
        
        if GameOverValues.passedScore > intHighScore{
            userDefaults.set(GameOverValues.passedScore, forKey: "highScore")
            intHighScore = GameOverValues.passedScore
            
        }
        yourScoreLabel.text = "\(GameOverValues.passedScore)"
        highScoreLabel.text = "\(intHighScore)"
        saveScoreToRealm()
    }
    
    func saveScoreToRealm(){
        let realm = try! Realm()
        let score = Score()
        score.name = GameValues.playerName
        score.score = GameOverValues.passedScore
        try! realm.write {
            realm.add(score)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
