//
//  HighScore+CoreDataProperties.swift
//  BubbleSmash
//
//  Created by Nickita on 30/4/17.
//  Copyright © 2017 Nickita. All rights reserved.
//

import Foundation
import CoreData


extension HighScore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HighScore> {
        return NSFetchRequest<HighScore>(entityName: "HighScore");
    }

    @NSManaged public var name: String?
    @NSManaged public var score: Int32

}
